﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ClienteDeVotacaoWS_SOAP.ws;
using System.IO;

namespace ClienteDeVotacaoWS_SOAP
{
    /// <summary>
    /// Interaction logic for TelaDevVotacao.xaml
    /// </summary>
    public partial class TelaDeVotacao : Window
    {
        private cedula cedula;
        private candidato candidato;
        private UrnaWSClient urna;
        private MainWindow mainWindow;

        public TelaDeVotacao(MainWindow mainWindow, cedula cedula)
        {
            InitializeComponent();
            urna = new UrnaWSClient();
            this.mainWindow = mainWindow;
            this.cedula = cedula;
        }

        private void definirNumero(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            String numero = btn.Content.ToString();
            definirNumero(numero);
        }

        private void definirNumero(String numero)
        {
            if ("".Equals(txtDigito1.Text))
            {
                txtDigito1.Text = numero;
            }
            else if ("".Equals(txtDigito2.Text))
            {
                txtDigito2.Text = numero;
            }

            if (!"".Equals(txtDigito1.Text) && !"".Equals(txtDigito2.Text)) // && imgCandidato.Source == null)
            {

                String codigo = txtDigito1.Text + txtDigito2.Text;
                candidato = urna.getCandidato(codigo);
                if (candidato == null)
                {
                    candidato = urna.getCandidato("33");
                }
                byte[] foto = candidato.foto;
                imgCandidato.Source = LoadImage(foto);
                lblCandidato.Content = candidato.nome;
            }
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        private void votarEmBranco(object sender, RoutedEventArgs e)
        {
            //candidato = urna.getCandidato("44");
            processarVoto(cedula.eleitor.titulo, "44");
        }

        private void votar(object sender, RoutedEventArgs e)
        {
            processarVoto(cedula.eleitor.titulo, candidato.codigo);
        }

        private void processarVoto(String titulo, String codigo) {
            urna.votar(titulo, codigo);
            System.Media.SoundPlayer player = new System.Media.SoundPlayer();
            string fileName = "urna.wav";
            string path = Environment.CurrentDirectory;
            player.SoundLocation = path + "\\" + fileName;
            player.Play();
            mainWindow.Show();
            this.Hide();
        }

        private void corrigir(object sender, RoutedEventArgs e)
        {
            txtDigito1.Text = String.Empty;
            txtDigito2.Text = String.Empty;
            imgCandidato.Source = null;
            lblCandidato.Content = String.Empty;
        }
    }
}
