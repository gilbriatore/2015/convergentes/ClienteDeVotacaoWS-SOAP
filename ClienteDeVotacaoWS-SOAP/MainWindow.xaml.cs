﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ClienteDeVotacaoWS_SOAP.ws;

namespace ClienteDeVotacaoWS_SOAP
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private UrnaWSClient urna;
        public MainWindow()
        {
            InitializeComponent();
            urna = new UrnaWSClient();
        }

        private void votar(object sender, RoutedEventArgs e)
        {
            String titulo = txtTitulo.Text;
            cedula cedula = urna.getCedula(titulo);

            if (cedula != null)
            {
                lblInvalido.Visibility = Visibility.Hidden;
                txtTitulo.Text = String.Empty;
                TelaDeVotacao tela = new TelaDeVotacao(this,cedula);
                tela.Show();
                this.Hide();
            }
            else
            {
                lblInvalido.Visibility = Visibility.Visible;
            }
        }
    }
}
